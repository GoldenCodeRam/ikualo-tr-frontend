/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_BACKEND_URL: string
  readonly VITE_ARTIFICIAL_LOADING_TIME: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
