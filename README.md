# ikualo-tr-frontend

A continuación se presenta la solución al desafío técnico ofrecido para la
posición de _Programador Junior_. Esta solución se encuentra dividida en dos
proyectos; _Backend_ y _Frontend_. Este es el proyecto _Frontend_.

## Requerimientos del Proyecto

A continuación se muestran los requerimientos directos del proyecto:

 - Crear una interfaz de usuario para mostrar la lista de movimientos.
 - Permitir a los usuarios agregar nuevos movimientos mediante un formulario.
 - Mostrar el capital actual del usuario actualizado según sus movimientos.

Por otra parte, se especificó que era posible realizar el proyecto haciendo uso
de los _frameworks_ web [React](https://react.dev/) o [Vue.js](https://vuejs.org/).
Se seleccionó _Vue.js_ para la creación del proyecto.

## Funcionalidades del Proyecto

A continuación se presentan las funcionalidades del proyecto como historias de usuario. La descripción de otras funcionalidades y características del proyecto no enunciadas en los requerimientos directos del proyecto se mostrarán en la sección de [Funcionalidades Adicionales del Proyecto](#funcionalidades-adicionales-del-ployecto).

| Historia de Usuario | Descripción |
| - | - |
| Como usuario, deseo ingresar a la aplicación con un usuario y con una contraseña, para así poder almacenar mis movimientos financieros. | Finalizado. |
| Como usuario, deseo observar mi información, como lo son mi usuario, mi capital y mis movimientos, para poder ver cómo se está almacenando todo en la aplicación. | Finalizado. |
| Como usuario, deseo observar los cambios que se realizan a mi capital con los movimientos que realizo. Esto con el fin de poder ver fácilmente la interación que tengo con la aplicación. | Finalizado |
| Como usuario, deseo eliminar los movimientos ya registrados, así, si cometo algún error o si un movimiento es erróneo, lo podré corregir. | Finalizado. |
| Como usuario, deseo observar la fecha, el tipo, el valor y la descripción de los movimientos, para así llevar un mejor control de ellos. | Finalizado. |
| Como usuario, deseo añadir nuevos movimientos, registrando el tipo, valor y descripción del movimiento. | Finalizado. |

### Funcionalidades Adicionales del Proyecto

A continuación se presentan las funcionalidades adicionales del proyecto como una lista de tareas. Se decide usar este formato porque estas funcionalidades puede que no se encuentren desarrolladas en su totalidad, puedan requerir cambios o se hayan implementado con el fin de mostrar posibles mejoras a futuro.

 - [x] Diálogos.
 - Se han creado diálogos que muestran el estado de la aplicación.
 - En algunos casos se podrían usar notificaciones de tipo [Toast](https://developer.android.com/guide/topics/ui/notifiers/toasts.html).
 - La eliminación de un movimiento se realiza a través de un diálogo, pero la creación de un movimiento se realiza en una ruta dedicada.
 - Se podrían realizar optimizaciones a la forma como se realizan las notificaciones, pero son consideraciones que no se tendrán en cuenta para la entrega.
 - [ ] Registro de Usuarios.
 - No se tiene un registro de usuarios.
 - Si bien se podría implementar un registro de usuarios, esto requeriría la validación de contraseñas, la recuperación de una cuenta, edición de la cuenta, etc. De manera que no se tendrá en cuenta para la entrega.
 - [ ] Edición y Actualización de Movimientos.
 - No se tiene una edición o actualización de movimientos.
 - Son funcionalidades sencillas de agregar, pero se decide no agregar por el momento, para reducir los tiempos de entrega. No se tendrá en cuenta para la entrega.
 - [x] Tutoriales.
 - Se decide agregar una librería de tutoriales que muestran el funcionamiento general de la aplicación para mejorar la usabilidad de la misma.
 - Por el momento no se tiene una manera de repetir los tutoriales. No se tendrá en cuenta para la entrega.
 - [ ] Pruebas Unitarias.
 - No se tendrán en cuenta para la entrega.
 - [ ] Pruebas Funcionales.
 - No se tendrán en cuenta para la entrega.


## Despliegue

Para el despliegue de desarrollo de la aplicación se está usando el gestor de paquetes [NPM](https://www.npmjs.com/), junto con [Vite](https://vitejs.dev/).

### Dependencias

Como parte del entorno de desarrollo, se está utilizando [Nix](https://nixos.org/) para el manejo de las dependencias. Sin embargo, si no se desea utilizar este gestor de paquetes, a continuación se muestran las dependencias principales del proyecto.

```
node v21.7.2
npm v10.5.0
```

Luego de clonar el proyecto, se deberían ejecutar los siguientes comandos:

```sh
# Instalación de paquetes del proyecto.
npm install
```

El proyecto hace uso de una serie de variables de entorno, y su configuración se muestra a continuación:

```sh
# Se recomienda copiar el archivo de ejemplo.
cp .env.example .env
```

| Variable | Descripción | Valor por Defecto |
| - | - | - |
| `VITE_BACKEND_URL` | Define la URL al servidor _Backend_. | `http://localhost:3000` |
| `VITE_ARTIFICIAL_LOADING_TIME` | Define los tiempos de carga artificiales para mostrar diálogos de información en la aplicación. | `2000` |

Finalmente, se puede ejecutar el entorno de desarrollo para observar el funcionamiento de la aplicación:

```sh
npm run dev
```

Si todos los valores se manejan por defecto, la aplicación se encontrará desplegada en la dirección: http://localhost:5173

