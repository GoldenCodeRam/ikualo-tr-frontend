export type AuthUser = {
    sub: string;
};

export type User = {
    email: string;
};

const MovementType = {
    in: "in",
    out: "out",
} as const;
export type MovementType = keyof typeof MovementType;

export type Movement = {
    id: string;
    date: string;
    type: MovementType;
    value: number;
    description?: string;
};

export type Toast = {
    message: string;
}
