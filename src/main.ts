import "./assets/main.css";

import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";
import { createI18n } from "vue-i18n";

import esCommon from "./assets/locales/es/common.json";
import esOnboarding from "@/assets/locales/es/onboarding.json";

const i18n = createI18n({
    legacy: false,
    locale: "es",
    messages: {
        es: {
            ...esCommon,
            onboarding: esOnboarding,
        },
    },
});

const app = createApp(App);

app.use(i18n);
app.use(createPinia());
app.use(router);

app.mount("#app");
