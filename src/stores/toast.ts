import type { Toast } from "@/types";
import { defineStore } from "pinia";

export const useToastStore = defineStore<string, { toasts: Toast[] }>("toast", {
    state: () => {
        return {
            toasts: [],
        };
    },
});
