import { defineStore } from "pinia";
import type { StepEntity } from "v-onboarding";

const OnboardingType = {
    DASHBOARD: "DASHBOARD",
} as const;
export type OnboardingType = keyof typeof OnboardingType;

export const useOnboardingStore = defineStore<
    string,
    { steps: StepEntity[]; onboardings: OnboardingType[] }
>("onboarding", {
    state: () => {
        return {
            steps: [],
            onboardings: ["DASHBOARD"],
        };
    },
    actions: {
        isOnboardingEnabled(onboarding: OnboardingType) {
            return !sessionStorage.getItem(onboarding);
        },
        enableOnboarding(onboarding: OnboardingType) {
            sessionStorage.removeItem(onboarding);
        },
        disableOnboarding(onboarding: OnboardingType) {
            sessionStorage.setItem(onboarding, onboarding);
        },
    },
});
