import type { User } from "@/types";
import { defineStore } from "pinia";

export const useUserStore = defineStore<string, User>("user", {
    state: () => {
        return {
            email: "",
        };
    },
});
