import { createRouter, createWebHistory } from "vue-router";
import { me } from "@/services/api/auth";

import LoginView from "../views/LoginView.vue";
import DashboardView from "../views/DashboardView.vue";
import MovementsCreateView from "@/views/MovementsCreateView.vue";
import { useUserStore } from "@/stores/user";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: "/login",
            name: "login",
            component: LoginView,
        },
        {
            path: "/dashboard",
            name: "dashboard",
            component: DashboardView,
        },
        {
            path: "/movements/create",
            name: "movements-create",
            component: MovementsCreateView,
        },
        {
            path: "/:pathMatch(.*)*",
            redirect: "login",
        },
    ],
});

// Router Navigation Guard
router.beforeEach(async (to, _from) => {
    const user = useUserStore();
    const authUser = await me();

    // Make sure the user is authenticated and avoid infinite redirect.
    if (!authUser && to.name !== "login") {
        return { name: "login" };
    }

    // Set the user information every time we get the information from the server.
    if (authUser) {
        user.email = authUser.sub;
    }
});

export default router;
