import type { StepEntity } from "v-onboarding";
import type { ComposerTranslation } from "vue-i18n";

export namespace DashboardOnboardingSteps {
    export function createCurrentCapitalStep(
        t: ComposerTranslation,
    ): StepEntity {
        return {
            attachTo: { element: "#current-capital" },
            content: {
                title: t("onboarding.current-capital.title"),
                description: t("onboarding.current-capital.description"),
            },
        };
    }

    export function createUserInfoStep(t: ComposerTranslation): StepEntity {
        return {
            attachTo: { element: "#user-info" },
            content: {
                title: t("onboarding.user-info.title"),
                description: t("onboarding.user-info.description"),
            },
        };
    }
}
