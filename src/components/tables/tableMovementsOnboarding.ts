import type { StepEntity } from "v-onboarding";
import type { ComposerTranslation } from "vue-i18n";

export namespace TableMovementsOnboardingSteps {
    export function createMovementsTableStep(
        t: ComposerTranslation,
    ): StepEntity {
        return {
            attachTo: { element: "#movements-table" },
            content: {
                title: t("onboarding.movements-table.title"),
                description: t("onboarding.movements-table.description"),
            },
        };
    }

    export function createMovementsTableActionStep(
        t: ComposerTranslation,
    ): StepEntity {
        return {
            attachTo: { element: "#movements-table-actions" },
            content: {
                title: t("onboarding.movements-table-actions.title"),
                description: t(
                    "onboarding.movements-table-actions.description",
                ),
            },
        };
    }
}
