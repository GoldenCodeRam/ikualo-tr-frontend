import { useUserStore } from "@/stores/user";
import type { AuthUser } from "@/types";
import axios from "axios";

const URLS = {
    LOGIN: import.meta.env.VITE_BACKEND_URL + "/auth/login",
    ME: import.meta.env.VITE_BACKEND_URL + "/auth/me",
} as const;
type URLS = keyof typeof URLS;

export async function login(
    email: string,
    password: string,
): Promise<AuthUser | undefined> {
    try {
        const result = await axios.post(
            URLS.LOGIN,
            {
                email,
                password,
            },
            {
                withCredentials: true,
            },
        );

        return result.data;
    } catch {
        return undefined;
    }
}

export async function me(): Promise<AuthUser | undefined> {
    try {
        const result = await axios.get(URLS.ME, { withCredentials: true });

        return result.data;
    } catch {
        return undefined;
    }
}
