import type { Movement, MovementType } from "@/types";
import axios from "axios";

const URLS = {
    BASE: import.meta.env.VITE_BACKEND_URL + "/movements",
    TOTAL_CAPITAL: import.meta.env.VITE_BACKEND_URL + "/movements/total-capital",
} as const;
type URLS = keyof typeof URLS;

export async function getAllMovements() {
    try {
        const result = await axios.get(URLS.BASE, { withCredentials: true });
        return result.data;
    } catch {
        return [];
    }
}

export async function getTotalCapital() {
    try {
        const result = await axios.get(URLS.TOTAL_CAPITAL, { withCredentials: true });
        return result.data;
    } catch {
        return undefined;
    }
}

export async function createMovement(
    value: number,
    type: MovementType,
    description: string,
): Promise<Movement | undefined> {
    try {
        const result = await axios.post(
            URLS.BASE,
            {
                value,
                type,
                description,
            },
            {
                withCredentials: true,
            },
        );
        return result.data;
    } catch {
        return undefined;
    }
}

export async function deleteMovement(
    movement: Movement,
): Promise<Movement | undefined> {
    try {
        const result = await axios.delete(URLS.BASE, {
            withCredentials: true,
            data: {
                id: movement.id,
            },
        });
        return result.data;
    } catch {
        return undefined;
    }
}
